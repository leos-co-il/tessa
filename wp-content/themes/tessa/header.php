<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$logo = opt('logo');
$facebook = opt('facebook');
$linkedin = opt('linkedin');
$whatsapp = opt('whatsapp');
?>

<header>
	<div class="drop-menu">
		<nav class="drop-nav">
			<?php getMenu('header-menu', '2', '', ''); ?>
			<div class="d-flex align-items-center justify-content-start">
				<?php if ($facebook) : ?>
					<a href="<?= $facebook; ?>" class="social-link menu-social">
						<i class="fab fa-facebook-f"></i>
					</a>
				<?php endif; ?>
				<?php if ($linkedin) : ?>
					<a href="<?= $linkedin; ?>" class="social-link menu-social">
						<i class="fab fa-linkedin-in"></i>	</a>
				<?php endif;
				if ($whatsapp) : ?>
					<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link menu-social">
						<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp-icon">
					</a>
				<?php endif; ?>
			</div>
		</nav>
	</div>
    <div class="container">
        <div class="row align-items-center justify-content-between">
			<div class="col">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
            <div class="col-xl-3 col-md-4 col-6">
				<?php if ($logo) : ?>
				<a href="/" class="logo">
					<img src="<?= $logo['url'] ?>" alt="logo">
				</a>
				<?php endif; ?>
			</div>
			<div class="col d-flex justify-content-end align-items-center">
				<?php do_action( 'wpml_footer_language_selector'); ?>
			</div>
        </div>
    </div>
</header>

<div class="triggers-col">
	<div class="pop-trigger">
		<img src="<?= ICONS.'trigger-pop.png'; ?>" alt="popup-trigger">
	</div>
	<?php if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="social-link">
			<i class="fab fa-facebook-f"></i>
		</a>
	<?php endif; ?>
	<?php if ($linkedin) : ?>
		<a href="<?= $linkedin; ?>" class="social-link">
			<i class="fab fa-linkedin-in"></i>	</a>
	<?php endif;
	if ($whatsapp) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link">
			<img src="<?= ICONS ?>whatsapp-blue.png" alt="whatsapp-icon">
		</a>
	<?php endif; ?>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="pop-form-col">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
					<?php if ($logo) : ?>
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					<?php endif;
					if ($f_title = opt('pop_form_title')) : ?>
						<h2 class="pop-form-title"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_subtitle = opt('pop_form_subtitle')) : ?>
						<h3 class="pop-form-subtitle"><?= $f_subtitle; ?></h3>
					<?php endif;
					if ($f_text = opt('pop_form_text')) : ?>
						<p class="base-text mb-3"><?= $f_text; ?></p>
					<?php endif;
					lang_form(['he' => '34', 'en' => '28'], '28') ?>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>
