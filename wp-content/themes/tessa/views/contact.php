<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel_1 = opt('tel');
$tel_2 = opt('tel_2');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<article class="page-body contact-page">
	<div class="about-main" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="about-overlay reviews-page-main">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="reviews-content-block">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-lg-10 col-md-11 col-12 mb-3">
					<div class="block-title-wrap">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<?php if ($tel_1 || $tel_2) : ?>
					<div class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
						 data-wow-delay="0.2s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-tel.png">
						</div>
						<h3 class="contact-title-item">
							<?= lang_text(['he' => 'התקשר אלינו', 'en' => 'Call Us'], 'en')?>
						</h3>
						<div class="d-flex align-items-center justify-content-center">
							<a href="tel:<?= $tel_1; ?>" class="contact-info">
								<?= $tel_1; ?>
							</a>
							<span class="base-text px-1">|</span>
							<a href="tel:<?= $tel_2; ?>" class="contact-info">
								<?= $tel_2; ?>
							</a>
						</div>
					</div>
				<?php endif;
				if ($mail) : ?>
					<div class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
						 data-wow-delay="0.4s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-mail.png">
						</div>
						<h3 class="contact-title-item">
							<?= lang_text(['he' => 'אימייל', 'en' => 'Email'], 'en')?>
						</h3>
						<a href="mailto:<?= $mail; ?>" class="contact-info">
							<?= $mail; ?>
						</a>
					</div>
				<?php endif;
				if ($address) : ?>
					<div class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.6s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-geo.png">
						</div>
						<h3 class="contact-title-item">
							<?= lang_text(['he' => 'מצא אותנו', 'en' => 'Find Us'], 'en')?>
						</h3>
						<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
							<?= $address; ?>
						</a>
					</div>
				<?php endif;
				if ($open_hours) : ?>
					<div class="contact-item-link contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-hours.png">
						</div>
						<h3 class="contact-title-item">
							<?= lang_text(['he' => 'שעות פתיחה', 'en' => 'Opening Hours'], 'en')?>
						</h3>
						<p class="contact-info">
							<?= $open_hours; ?>
						</p>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="container my-5">
		<div class="row justify-content-center align-items-center">
			<div class="<?= $fields['contact_map'] ? 'col-lg-7' : 'col-lg-12' ?>">
				<div class="d-flex flex-column align-items-start justify-content-center">
					<?php if ($fields['contact_form_title'] || $fields['contact_form_subtitle']) : ?>
						<div class="mid-form-title-wrap">
							<?php if ($fields['contact_form_title']) : ?>
								<h2 class="base-form-title"><?= $fields['contact_form_title']; ?></h2>
							<?php endif;
							if ($fields['contact_form_subtitle']) : ?>
								<h3 class="base-form-subtitle"><?= $fields['contact_form_subtitle']; ?></h3>
							<?php endif; ?>
						</div>
					<?php endif;
					if ($fields['contact_form_text']) : ?>
						<p class="base-text mb-3"><?= $fields['contact_form_text']; ?></p>
					<?php endif;
					lang_form(['he' => '35', 'en' => '29'], '29') ?>
				</div>
			</div>
			<?php if ($fields['contact_map']) : ?>
				<div class="col-lg-5">
					<div class="contact-map-wrap">
						<img src="<?= $fields['contact_map']['url']; ?>" alt="map">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_footer(); ?>
