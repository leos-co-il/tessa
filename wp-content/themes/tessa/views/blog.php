<?php
/*
Template Name: מאמרים
*/
$post_type = (isset($fields['type']) && ($fields['type'] === 'service')) ? 'service' : 'post';
get_header();
$fields = get_fields();
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => $post_type,
	'suppress_filters' => false
]);
?>
<article class="page-body">
	<div class="about-main" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="about-overlay reviews-page-main">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="reviews-content-block">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-lg-10 col-md-11 col-12 mb-3">
					<div class="block-title-wrap">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php if ($posts) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($posts as $x => $post) : ?>
						<div class="col-lg-3 col-sm-6 col-12 review-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'service', [
									'post' => $post,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form_base');
if ($fields['page_review_item'] || $fields['page_rev_block_text']) : ?>
	<section class="reviews-block mb-5">
		<?php if ($fields['page_rev_block_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['page_rev_block_text'],
			]);
		}
		if ($fields['page_review_item']) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['page_review_item'] as $x => $review) : ?>
						<div class="col-lg-4 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'review', [
									'review' => $review,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

