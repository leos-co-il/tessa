<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();

?>

<article class="about-body">
	<div class="about-main" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="about-overlay">
			<div class="container">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start bread-row bread-row-body">
						<div class="col-12">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
						<div class="block-title-wrap">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['about_video_link']) : ?>
		<div class="container">
			<?php if ($fields['about_video_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="about-video-title"><?= $fields['about_video_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-7 col-md-8 col-12">
					<div class="about-video"
						 style="background-image: url('<?= getYoutubeThumb($fields['about_video_link']); ?>')">
						<span class="play-video" data-video="<?= getYoutubeId($fields['about_video_link']); ?>">
							<img src="<?= ICONS ?>play.png" class="play-icon" alt="play-video">
						</span>
					</div>
				</div>
			</div>
		</div>
	<?php get_template_part('views/partials/repeat', 'video_modal');
	endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'form_color');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
