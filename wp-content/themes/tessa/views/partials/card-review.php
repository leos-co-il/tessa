<?php if (isset($args['review']) && $args['review']) : ?>
	<div class="review-item">
		<div class="rev-pop-trigger">
			<div class="hidden-review">
				<div class="slider-output">
					<?= $args['review']['rev_text']; ?>
				</div>
			</div>
		</div>
		<?php if ($args['review']['rev_logo']) : ?>
			<div class="review-image-wrap">
				<div class="review-img">
					<img src="<?= $args['review']['rev_logo']['url']; ?>">
				</div>
			</div>
		<?php endif; ?>
		<div class="rev-content">
			<div class="review-prev">
				<?= text_preview($args['review']['rev_text'], '30'); ?>
			</div>
		</div>
	</div>
<!--Reviews pop-up-->
<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i class="fas fa-times"></i>
			</button>
			<div class="modal-body" id="reviews-pop-wrapper"></div>
		</div>
	</div>
</div>
<?php endif; ?>
