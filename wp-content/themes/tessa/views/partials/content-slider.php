<?php if (isset($args['content']) && ($args['content'])) :?>
	<div class="base-slider-block custom-arrows">
		<?php if (isset($args['img']) && ($args['img'])) : ?>
			<div class="slider-col-img" style="background-image: url('<?= $args['img']['url']; ?>')">
			</div>
		<?php endif; ?>
		<div class="slider-col-content <?= isset($args['img']) && ($args['img']) ? '' : 'max-col-content'; ?>">
			<div class="base-slider" dir="rtl">
				<?php foreach ($args['content'] as $content) : ?>
					<div class="slider-base-item">
						<div class="slider-output">
							<?= $content['content']; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

