<?php if (isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']);
	$icon = get_field('serv_icon', $args['post']); ?>
	<div class="post-card-column">
		<a class="post-img" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif; ?>>
			<span class="post-overlay">
				<?php if ($icon) : ?>
					<img src="<?= $icon['url']; ?>" alt="service-icon">
				<?php endif; ?>
			</span>
		</a>
		<div class="post-card-content">
			<h3 class="post-card-title"><?= $args['post']->post_title; ?></h3>
			<p class="post-card-text">
				<?= text_preview($args['post']->post_content, 20); ?>
			</p>
		</div>
		<a class="base-link" href="<?= $link; ?>">
			<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Read more'], 'en'); ?>
		</a>
	</div>
<?php endif; ?>
