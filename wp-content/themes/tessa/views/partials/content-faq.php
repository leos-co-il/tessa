<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-sm-10 col-12 block-title-wrap">
					<h2>
						<?= (isset($args['title']) && $args['title']) ? $args['title'] :
							lang_text(['he' => 'אתה צריך לדעת משהו?', 'en' => 'Do You Need To Know Something?'], 'en') ?>
						<span class="faq-subtitle">
						<?= (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] :
								lang_text(['he' => 'אולי תמצא את תשובתך כאן', 'en' => 'Maybe You’ll Find Your Answer Here'], 'en') ?>
						</span>
					</h2>
				</div>
				<div class="col-xl-8 col-lg-10 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="btn question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<img class="q-icon" src="<?= ICONS ?>faq.png" alt="faq-icon">
										<?= $item['faq_question']; ?>
										<span class="arrow-top">
										</span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="answer-body slider-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
