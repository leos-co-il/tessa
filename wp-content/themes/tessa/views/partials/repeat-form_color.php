<section class="repeat-form-imaged" <?php if ($img = opt('rep_form_back_img')) : ?>
	style="background-image: url('<?= $img['url']; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="rep-form-col">
					<?php if ($f_title = opt('rep_form_title')) : ?>
						<h2 class="block-title mb-3"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_text = opt('rep_form_text')) : ?>
						<p class="block-text"><?= $f_text; ?></p>
					<?php endif;
					lang_form(['he' => '32', 'en' => '31'], '31') ?>
				</div>
			</div>
		</div>
	</div>
</section>
