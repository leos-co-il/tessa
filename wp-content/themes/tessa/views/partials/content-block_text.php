<div class="container">
	<div class="row justify-content-center">
		<div class="col-xl-7 col-lg-9 col-sm-10 col-12">
			<?php if(isset($args['text']) && $args['text']) : ?>
				<div class="block-title-wrap">
					<?= $args['text']; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if (isset($args['link']) && $args['link']) : ?>
		<div class="row justify-content-center">
			<div class="col-auto">
				<a class="base-link mb-4" href="<?= isset($args['link']['title']) ?? $args['link']['title']; ?>">
					<?php
					$text_link = (isset($args['link']['title']) && $args['link']['title']) ? $args['link']['title'] :
						lang_text(['he' => 'קרא עוד', 'en' => 'Read more'], 'en');
					echo $text_link; ?>
				</a>
			</div>
		</div>
	<?php endif; ?>
</div>
