<?php if (isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']);
	$icon = get_field('serv_icon', $args['post']); ?>
	<div class="post-card-column home-post-card-column">
		<a class="post-img home-post-img" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?>>
		</a>
		<div class="post-card-content home-post-card-content">
			<h3 class="home-post-card-title"><?= $args['post']->post_title; ?></h3>
			<p class="block-text">
				<?= text_preview($args['post']->post_content, 20); ?>
			</p>
		</div>
		<a class="base-link" href="<?= $link; ?>">
			<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Read more'], 'en'); ?>
		</a>
	</div>
<?php endif; ?>
