<?php
$f_title = opt('mid_form_title');
$f_subtitle = opt('mid_form_subtitle'); ?>
<section class="repeat-form-base">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-9 col-sm-10 col-12">
				<div class="rep-form-col">
					<?php if ($f_title || $f_subtitle) : ?>
						<div class="mid-form-title-wrap">
							<?php if ($f_title) : ?>
								<h2 class="base-form-title"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_subtitle) : ?>
								<h3 class="base-form-subtitle"><?= $f_subtitle; ?></h3>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if ($f_text = opt('mid_form_text')) : ?>
						<p class="base-text mb-3"><?= $f_text; ?></p>
					<?php endif;
					lang_form(['he' => '33', 'en' => '30'], '30') ?>
				</div>
			</div>
		</div>
	</div>
</section>
