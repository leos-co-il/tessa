<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();

?>

<article class="post-body">
	<div class="about-main post-main-img" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="about-overlay reviews-page-main post-block-main">
			<div class="container">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start bread-row bread-row-body">
						<div class="col-12">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
						<div class="project-output text-center">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['project_item']) : ?>
		<div class="reviews-content-block gallery-page-block">
			<div class="container">
				<?php foreach ($fields['project_item'] as $key => $project) : ?>
					<div class="project-item-block">
						<div class="row justify-content-center align-items-start pr-row">
							<?php if ($project['project_content']) : ?>
								<div class="col-lg-6">
									<div class="project-output">
										<?= $project['project_content']; ?>
									</div>
								</div>
							<?php endif;
							if ($project['project_photos']) : ?>
								<div class="col-lg-6 gallery-line justify-content-end project-item-images">
									<?php foreach ($project['project_photos'] as $x => $gallery_item) : ?>
										<div class="gallery-item-wrap small-pr-photo">
											<a class="gallery-item project-photo wow zoomIn"
											   data-wow-delay="0.<?= $x * 2; ?>s"
											   style="background-image: url('<?= $gallery_item['url']; ?>')"
											   href="<?= $gallery_item['url']; ?>" data-lightbox="home-gallery">
												<img src="<?= ICONS ?>gal-plus.png" class="gallery-plus" alt="gal-plus">
												<span class="gallery-overlay"></span>
											</a>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div>
						<?php if ($project['project_gal_item']) :
							get_template_part('views/partials/repeat', 'video_modal'); ?>
							<div class="row">
								<div class="col-12 more-gallery-row">
									<div class="gallery-link gallery-link-close <?= ($key > 0) ? 'hide-line' : ''; ?>">
										<?= lang_text(['he' => 'סגור גלריה', 'en' => 'close gallery'], 'en'); ?>
									</div>
									<div class="gallery-link gallery-link-open <?= ($key === 0) ? 'hide-line' : ''; ?>">
										<?= lang_text(['he' => 'גלריה מורחבת', 'en' => 'Extended gallery'], 'en'); ?>
									</div>
									<div class="gallery-line-wrap <?= ($key > 0) ? 'hide-line' : ''; ?> w-100">
										<div class="gallery-line w-100">
											<?php foreach ($project['project_gal_item'] as $x => $gal_item) : ?>
												<div class="gallery-item-wrap">
													<?php if (($gal_item['acf_fc_layout'] === 'home_gal_img') &&
															(isset($gal_item['gal_img']) && $gal_item['gal_img'])) : ?>
														<a class="gallery-item project-photo"
														   style="background-image: url('<?= $gal_item['gal_img']['url']; ?>')"
														   href="<?= $gal_item['gal_img']['url']; ?>" data-lightbox="project-gallery">
															<img src="<?= ICONS ?>gal-plus.png" class="gallery-plus" alt="gal-plus">
															<span class="gallery-overlay"></span>
														</a>
													<?php elseif (($gal_item['acf_fc_layout'] === 'home_gal_video') &&
															(isset($gal_item['gal_video']) && $gal_item['gal_video'])): ?>
														<a class="gallery-item" style="background-image: url('<?= getYoutubeThumb($gal_item['gal_video']); ?>')">
															<img src="<?= ICONS ?>gal-plus.png" class="gallery-plus" alt="plus">
															<span class="play-video" data-video="<?= getYoutubeId($gal_item['gal_video']); ?>">
														<img src="<?= ICONS ?>play.png" class="play-icon" alt="play-video">
													</span>
														</a>
													<?php endif; ?>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form_base');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
