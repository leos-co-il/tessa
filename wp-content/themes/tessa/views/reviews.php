<?php
/*
Template Name: המלצות
*/

get_header();
$fields = get_fields();

?>

<article class="page-body">
	<div class="about-main" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="about-overlay reviews-page-main">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-10 col-12">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="reviews-content-block">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div class="block-title-wrap">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php if ($fields['main_review_item']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['main_review_item'] as $x => $review) : ?>
						<div class="col-lg-4 col-sm-6 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'review', [
									'review' => $review,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'form_base');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
}
get_footer(); ?>
