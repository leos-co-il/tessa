<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

if ($fields['home_slider']) : ?>
	<section class="home-main-block">
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['home_slider'] as $slide) : ?>
				<div class="slide-home" <?php if ($slide['home_img']): ?>
					style="background-image: url('<?= $slide['home_img']['url']; ?>')"
				<?php endif; ?>>
					<div class="main-slide-item">
						<div class="container">
							<div class="row justify-content-center">
								<div class="col-auto">
									<?php if ($slide['home_text']) : ?>
										<div class="block-text-home">
											<?= $slide['home_text']; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_services'] || $fields['home_services_text']) : ?>
	<section class="reviews-block services-mt mb-5">
		<?php if ($fields['home_services_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_services_text'],
					'link' => $fields['home_services_link'],
			]);
		}
		if ($fields['home_services']) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['home_services'] as $x => $service) : ?>
						<div class="col-lg-3 col-sm-6 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'service', [
									'post' => $service,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form_base');
if ($fields['home_about_text'] || $fields['home_about_img']) : ?>
	<section class="home-about" <?php if ($fields['home_about_img']) : ?>
	style="background-image: url('<?= $fields['home_about_img']['url']; ?>')"
		<?php endif; ?>>
		<div class="container">
			<div class="row justify-content-end align-items-stretch about-row">
				<?php if ($fields['home_about_text']) : ?>
					<div class="<?= $fields['home_about_img'] ? 'col-xl-6 col-lg-7' : 'col-xl-11'; ?> d-flex flex-column align-items-start">
						<div class="block-title-wrap home-about-output">
							<?= $fields['home_about_text']; ?>
						</div>
						<?php if ($fields['home_about_link']) : ?>
							<a class="base-link" href="<?= $fields['home_about_link']['url']; ?>">
								<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title']) ?
										$fields['home_about_link']['title'] : lang_text(['he' => 'המשך קריאה', 'en' => 'Read more'], 'en'); ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if ($fields['home_about_img']) : ?>
					<div class="col-lg-5">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_why_title'] || $fields['home_why_img'] || $fields['home_why_item']) : ?>
	<section class="home-why-block">
		<div class="container">
			<div class="row justify-content-end align-items-center">
				<?php if ($fields['home_why_title'] || $fields['home_why_item']) : ?>
					<div class="<?= $fields['home_why_img'] ? 'col-xl-4 col-lg-6' : 'col-xl-11'; ?> d-flex flex-column align-items-start">
						<h2 class="home-why-title"><?= $fields['home_why_title']; ?></h2>
						<?php if ($fields['home_why_item']) : ?>
							<div class="why-items-block">
								<?php foreach ($fields['home_why_item'] as $y => $why_item) : ?>
									<div class="why-item wow fadeInUp" data-wow-delay="0.<?= $y + 1; ?>s">
										<h3 class="why-item-title"><?= $why_item['why_item_title']; ?></h3>
										<p class="small-text pad-for-why"><?= $why_item['why_item_text']; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if ($fields['home_why_img']) : ?>
					<div class="col-xl-7 col-lg-6 col-why-img">
						<div class="overlay-why"></div>
						<img src="<?= $fields['home_why_img']['url']; ?>" alt="team-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_agents_text'] || $fields['home_agents_img']) : ?>
	<section class="partners-block">
		<?php if ($fields['home_agents_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_agents_text'],
			]);
		} if ($fields['home_agents_img']) : ?>
			<div class="container mt-3">
				<div class="row justify-content-center">
					<div class="col-xl-10 col-md-11 col-12">
						<img src="<?= $fields['home_agents_img']['url'];  ?>" alt="agents" class="w-100">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<div class="container">
    <div class="row">
        <div class="col-12">
		</div>
    </div>
</div>
<?php
get_template_part('views/partials/repeat', 'form_color');
if ($fields['home_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['home_slider_seo'],
			'img' => $fields['home_slider_img'],
	]);
}
if ($fields['home_gal_item'] || $fields['home_gallery_text']) : ?>
	<section class="gallery-block-home mb-5">
		<?php if ($fields['home_gallery_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_gallery_text'],
					'link' => $fields['home_gallery_link'],
			]);
		}
		if ($fields['home_gal_item']) : ?>
			<div class="gallery-line">
				<?php foreach ($fields['home_gal_item'] as $x => $gallery_item) : ?>
					<div class="gallery-item-wrap">
						<?php if (($gallery_item['acf_fc_layout'] === 'home_gal_img') &&
								(isset($gallery_item['gal_img']) && $gallery_item['gal_img'])) : ?>
							<a class="gallery-item wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s"
							   style="background-image: url('<?= $gallery_item['gal_img']['url']; ?>')"
							href="<?= $gallery_item['gal_img']['url']; ?>" data-lightbox="home-gallery">
								<img src="<?= ICONS ?>gal-plus.png" class="gallery-plus" alt="gal-plus">
								<span class="gallery-overlay"></span>
							</a>
						<?php elseif (($gallery_item['acf_fc_layout'] === 'home_gal_video') &&
								(isset($gallery_item['gal_video']) && $gallery_item['gal_video'])): ?>
							<a class="gallery-item wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s"
							   style="background-image: url('<?= getYoutubeThumb($gallery_item['gal_video']); ?>')">
								<img src="<?= ICONS ?>gal-plus.png" class="gallery-plus" alt="plus">
								<span class="play-video" data-video="<?= getYoutubeId($gallery_item['gal_video']); ?>">
									<img src="<?= ICONS ?>play.png" class="play-icon" alt="play-video">
								</span>
							</a>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</section>
	<?php get_template_part('views/partials/repeat', 'video_modal');
endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
}
if ($fields['home_review_item'] || $fields['home_rev_block_text']) : ?>
<section class="reviews-block mb-5">
	<?php if ($fields['home_rev_block_text']) {
		get_template_part('views/partials/content', 'block_text', [
				'text' => $fields['home_rev_block_text'],
				'link' => $fields['home_rev_block_link'],
		]);
	}
	if ($fields['home_review_item']) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['home_review_item'] as $x => $review) : ?>
					<div class="col-lg-4 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
						<?php get_template_part('views/partials/card', 'review', [
								'review' => $review,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php endif;
if ($fields['home_posts'] || $fields['home_posts_text']) : ?>
	<section class="reviews-block mb-5">
		<?php if ($fields['home_posts_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_posts_text'],
					'link' => $fields['home_posts_link'],
			]);
		}
		if ($fields['home_posts']) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['home_posts'] as $x => $post) : ?>
						<div class="col-md-4 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
} ?>
<?php get_footer(); ?>
