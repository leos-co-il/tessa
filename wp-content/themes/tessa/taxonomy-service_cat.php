<?php

get_header();
$query = get_queried_object();
$img = get_field('cat_img', $query);
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'service',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'service_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
	<article class="page-body">
		<div class="container">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start">
					<div class="col-12 breadcol">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<div class="about-main" <?php if ($img) : ?>
			style="background-image: url('<?= $img['url']; ?>')"
		<?php endif; ?>>
			<div class="about-overlay reviews-page-main">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-xl-8 col-lg-10 col-12">
							<h2 class="about-page-title"><?= $query->name; ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="reviews-content-block">
			<div class="container">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start bread-row">
						<div class="col-12">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<div class="col-lg-10 col-md-11 col-12 mb-3">
						<div class="block-title-wrap">
							<?= category_description(); ?>
						</div>
					</div>
				</div>
				<?php if ($posts) : ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($posts as $x => $post) : ?>
							<div class="col-lg-3 col-sm-6 col-12 review-col wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
								<?php get_template_part('views/partials/card', 'service', [
									'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</article>
<?php
get_template_part('views/partials/repeat', 'form_base');
$reviews = get_field('page_review_item', $query);
$rev_text = get_field('page_rev_block_text', $query);
if ($reviews || $rev_text) : ?>
	<section class="reviews-block mb-5">
		<?php if ($rev_text) {
			get_template_part('views/partials/content', 'block_text', [
				'text' => $rev_text,
			]);
		}
		if ($reviews) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($reviews as $x => $review) : ?>
						<div class="col-lg-4 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'review', [
								'review' => $review,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>
