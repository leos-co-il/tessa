<?php
$facebook = opt('facebook');
$linkedin = opt('linkedin');
$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$map = opt('map_image');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="to-top">
			<span class="top-text">
				<?= lang_text(['he' => 'חזרה למעלה', 'en' => 'BACK TO TOP'], 'en'); ?>
			</span>
		</a>
		<?php if ($current_id !== $contact_id) : ?>
			<div class="foo-form-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-xl-8 col-lg-10 col-md-11 col-12">
							<div class="rep-form-col">
								<?php if ($f_title = opt('foo_form_title')) : ?>
									<h2 class="block-title mb-3"><?= $f_title; ?></h2>
								<?php endif;
								if ($f_text = opt('foo_form_text')) : ?>
									<p class="base-text text-center mb-4"><?= $f_text; ?></p>
								<?php endif;
								lang_form(['he' => '33', 'en' => '30'], 'en'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container mt-5">
			<div class="row justify-content-between">
				<div class="col-xl-2 col-lg-3 col-sm-6 col-12 foo-menu main-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'מפת אתר', 'en' => 'Website map'], 'en'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2', '',
								'main_menu h-100 text-right'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu links-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'מאמרים חשובים', 'en' => 'Important articles'], 'en'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey three-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-sm-6 col-12 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'צור קשר', 'en' => 'Contact us'], 'en'); ?>
					</h3>
					<div class="menu-border-top contact-menu-foo">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>form-tel.png">
										<span>
											<?= lang_text(['he' => 'טלפון:', 'en' => 'Phone:'], 'en').$tel; ?>
										</span>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<img src="<?= ICONS ?>form-mail.png">
										<?= lang_text(['he' => 'מייל:', 'en' => 'Email:'], 'en').$mail; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<img src="<?= ICONS ?>contact-geo.png">
										<span>
											<?= lang_text(['he' => 'כתובת:', 'en' => 'Address:'], 'en').$address; ?>
										</span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<?php if ($map) : ?>
						<div class="map-foo">
							<img src="<?= $map['url']; ?>" alt="map" class="w-100 mx-2">
						</div>
						<?php endif; ?>
						<div class="socials-footer">
							<h5 class="small-text">
								<?= lang_text(['he' => 'מצא אותנו כאן', 'en' => 'Find us here:'], 'en'); ?>
							</h5>
							<?php if ($facebook) : ?>
							<a href="<?= $facebook; ?>" class="foo-social-link">
								<i class="fab fa-facebook-f"></i>
							</a>
							<?php endif; ?>
							<?php if ($linkedin) : ?>
							<a href="<?= $linkedin; ?>" class="foo-social-link">
								<i class="fab fa-linkedin-in"></i>
							</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
