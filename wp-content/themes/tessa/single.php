<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="post-body">
	<div class="about-main post-main-img" <?php if ($fields['post_top_img']) : ?>
		style="background-image: url('<?= $fields['post_top_img']['url']; ?>')"
	<?php endif; ?>>
		<div class="about-overlay reviews-page-main post-block-main">
			<div class="container">
				<?php if ( function_exists('yoast_breadcrumb') ) : ?>
					<div class="row justify-content-start bread-row bread-row-body">
						<div class="col-12">
							<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="about-page-title"><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="reviews-content-block">
		<div class="container">
			<div class="row justify-content-between align-items-start">
				<div class="<?= has_post_thumbnail() ? 'col-lg-6' : 'col-lg-12'; ?>">
					<div class="project-output post-text-output">
						<?php the_content(); ?>
					</div>
				</div>
				<?php if (has_post_thumbnail()) : ?>
					<div class="col-xl-5 col-lg-6">
						<img src="<?= postThumb(); ?>" alt="post-img" class="post-img-page">
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-start mt-4">
				<div class="col-auto">
					<div class="socials-share">
						<p class="share-text">
							<?= lang_text(['he' => 'שיתוף', 'en' => 'Share'], 'en'); ?>
						</p>
						<a href="mailto:?&subject=&body=<?= $post_link; ?>" target="_blank"
						   class="share-link share-mail" role="link">
							<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
						   class="share-link share-whats" role="link">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-whatsapp">
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
						   class="share-link share-fb">
							<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
if ($fields['post_step']) : ?>
	<section class="steps-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-center">
						<?php foreach ($fields['post_step'] as $key => $step) : ?>
							<div class="col-lg-3 col-md-6 col-12 col-after-line">
								<div class="step-item wow fadeIn" data-wow-delay="0.<?= $key; ?>s">
									<div class="step-num-wrap">
										<?php if ($step['step_icon']) : ?>
											<img src="<?= $step['step_icon']['url']; ?>" alt="process-icon">
										<?php endif; ?>
									</div>
									<p class="base-text text-center">
										<?= $step['step_title']; ?>
									</p>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'subtitle' => $fields['faq_subtitle'],
					'faq' => $fields['faq_item'],
			]);
}
get_template_part('views/partials/repeat', 'form_base');
if ($fields['post_review_item'] || $fields['post_rev_block_text']) : ?>
	<section class="reviews-block mb-5">
		<?php if ($fields['post_rev_block_text']) {
			get_template_part('views/partials/content', 'block_text', [
				'text' => $fields['post_rev_block_text'],
			]);
		}
		if ($fields['post_review_item']) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['post_review_item'] as $x => $review) : ?>
						<div class="col-lg-4 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
							<?php get_template_part('views/partials/card', 'review', [
								'review' => $review,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
$text = '';
switch ($currentType) {
	case 'post':
		$taxname = 'category';
		$text = lang_text(['he' => 'מאמרים נוספים', 'en' => 'Other Articles That You Would Interested'], 'en');
		break;
	case 'service':
		$taxname = 'service_cat';
		$text = lang_text(['he' => 'שירותים נוספים', 'en' => 'Other Services That You Would Interested'], 'en');
		break;
	default:
		$currentType = 'post';
		$taxname = 'category';
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'post_type' => $currentType,
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => $taxname,
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => $currentType,
			'post__not_in' => array($postId),
	]);
}?>
<section class="reviews-block mb-5">
	<?php get_template_part('views/partials/content', 'block_text', [
			'text' => $fields['same_posts_text'] ?? $text,
	]);
	if ($samePosts) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $x => $service) : ?>
					<div class="col-lg-4 col-sm-11 col-12 review-col wow zoomIn" data-wow-delay="0.<?= $x * 2; ?>s">
						<?php get_template_part('views/partials/card', 'service', [
								'post' => $service,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
